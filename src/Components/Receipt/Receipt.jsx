import React from 'react'
import './Receipt.css'
import {connect} from 'react-redux';
import { ListGroup, ListGroupItem, Badge, FormGroup, Label, Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { cartlist, orderdone, edit_cart,delete_cart_one, delete_cart_all, favor_update , getFavour} from '../../actions';
import {bindActionCreators}from 'redux';

class Receipt extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item:{},
      itemindex:0,
      modal: false,
      itemtotalprice:0,
      oldpriceitem:0,
      datamodelcart:{},
      faverPrice:0
    }
    this.handlecount = this.handlecount.bind(this);
    this.handleClickRemove = this.handleClickRemove.bind(this);
    this.handleClickAdd = this.handleClickAdd.bind(this);
  };

  componentDidMount () {
    this.props.cartlist();
  }

  handlecount(e) {
    this.setState({count:e.target.value});
  }
  handleClickAdd() {
      this.state.item.qty = this.state.item.qty+1;
      this.state.itemtotalprice =Number(this.state.itemtotalprice )+Number(this.state.oldpriceitem)+Number(this.state.faverPrice);
      this.setState((prevState) => ({
          item: this.state.item,
          itemtotalprice:this.state.itemtotalprice
      }));
  }
  handleClickRemove() {
      if(this.state.item.qty!=0){
         this.state.itemtotalprice =Number(this.state.itemtotalprice)-Number(this.state.oldpriceitem)-Number(this.state.faverPrice);
          this.state.item.qty = this.state.item.qty-1;
          this.setState((prevState) => ({
              item: this.state.item,
              itemtotalprice:this.state.itemtotalprice
          }));
      }
  }

  openModel = async(item, index) => {
    console.log(item);
    // faverPrice
    await item.favours.some(el => {
      this.state.faverPrice += Number(el.price);
    })
    this.props.getFavour(item.favour);
    this.props.cartlist();
    this.setState(prevState => ({
      modal: !prevState.modal,
      item:item,
      itemindex:index,
      datamodelcart:item,
      itemtotalprice:item.totelprice,
      itemtotalpriceold:item.totelprice,
      oldpriceitem:item.price,
      faverPrice: this.state.faverPrice
    }));
  }

  editcartdata = (data) =>{
    this.props.edit_cart(data, this.state.itemindex);
    this.props.cartlist();
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
    
  }

  deletefavor = async(faverobj, array) =>{
     var favorindex = this.state.item.favours.indexOf(faverobj);
    await array.some(el => {
      el.totelprice -= Number(this.state.item.favours[favorindex].price)*Number(this.state.item.qty);
      el.favours.splice(favorindex,1);
     })
    this.state.itemtotalprice -=Number(Number(this.state.item.favours[favorindex].price)*Number(this.state.item.qty));
    this.state.item.favours.splice(favorindex,1);
    console.log(this.state.itemtotalprice);
    this.setState(prevState => ({
      item: this.state.item,
      itemtotalprice: this.state.itemtotalprice
    }));
    this.props.favor_update(array);
  }
	
  deleteOne = (item, index) => {
    this.props.delete_cart_one(item, index);
    this.props.cartlist();
  }
  deleteAll = () => {
    this.props.delete_cart_all();
    this.props.cartlist();
  }
  Orderplace = (item, price) =>{
    if(item.length!=0){
      if(navigator.onLine){
        this.props.orderdone(item, price, "create");
        localStorage.removeItem("cartdata");
        this.props.cartlist();
      }else{
        alert("Internet error");
      }
    }else{
      alert("No item add in cart");
    }
   
  }


  handleChangeCart(e, value, item, radioname, index, checktype) {
    if(checktype.type=="check"){
        if(e.target.checked==true){
            value.checked = e.target.checked;
            value.title= radioname;
            if(Number(value.price)!=0){
                this.state.itemtotalprice = Number(this.state.itemtotalprice)+Number(value.price);
            }else{
                this.state.itemtotalprice = Number(this.state.itemtotalprice);
            }
            item.favours.push(value);
            this.setState(prevState => ({
              datamodelcart: item,
                itemtotalprice: this.state.itemtotalprice
            }));
        }else{
            value.checked = e.target.checked;
            var index = item.favours.indexOf(value);
            if(Number(value.price)!=0){
                this.state.itemtotalprice = Number(this.state.itemtotalprice)-Number(value.price);
            }
            item.favours.splice(index,1);
            this.setState(prevState => ({
              datamodelcart: item,
                itemtotalprice: this.state.itemtotalprice
            }));
        }
    }else{
        if(item.favour[index].name==radioname){
           var check = this.state.datamodelcart.favours.some(el => el.title == radioname);
           if(!check){
               value.checked = true;
               value.title= radioname;
               if(Number(value.price)!=0){
                this.state.itemtotalprice = Number(this.state.itemtotalprice)+Number(value.price);
               }
               item.favours.push(value);
               this.setState(prevState => ({
                datamodelcart: item,
                   itemtotalprice: this.state.itemtotalprice
               }));
           }else{
               value.checked = e.target.checked;
               value.title= radioname;
               var index = item.favours.indexOf(value);
               this.state.itemtotalprice= Number(this.state.itemtotalpriceold)-Number(value.price);
               item.favours.splice(index,1, value);
               this.setState(prevState => ({
                datamodelcart: item,
                   itemtotalprice:this.state.itemtotalprice
               }));
           }
       }
       console.log(this.state.datamodelcart);
    }
}

  render(){
    const that = this;
    return (
      <div className="mb-50 padding-r">
        <h3>Receipt </h3>
        {/* <p>
            No: {this.state.ordernumber}
        </p> */}
        <Table>
            <thead>
            <tr>
                <th>Type</th>
                <th>Price</th>
            </tr>
            </thead>
              <tbody>
                {this.props.cartList?this.props.cartList.map((item, index)=> (
                    <tr key={item.id}>
                        <td>
                        {item.name} {'    '}
                        {item.favours.length!=0?<Button className="delete-btn boder-radius btn-right" color="success"><i className="fa fa-plus-circle" /></Button>:''}
                         </td>
                        <td>${Number(item.totelprice)}
                        </td>
                        <td><Button onClick={() => that.deleteOne(item, index)} className="float-left delete-btn boder-radius" color="danger"><i className="fa fa-close" /></Button>{' '}
                        <Button  onClick={() => that.openModel(item, index)} className="float-left delete-btn boder-radius" color="success"><i className="fa fa-edit" /></Button></td>
                    </tr>
                  )):null}
              </tbody>
            <thead>
            <tr>
                <th className="padding-th">
                <Button onClick={() => this.Orderplace(this.props.cartList, this.props.totalprice)} className="btn-right order-btn" color="primary">Confirm</Button>
                {/* <Button outline className="btn-right order-btn" color="primary">#{this.state.ordernumber}</Button> */}
                </th>
                <th className="padding-amount">${this.props.totalprice}</th>
                <th><Button onClick={() => this.deleteAll()} className="float-left btn-padding boder-radius" color="danger"><i className="fa fa-close" /></Button></th>
            </tr>
            </thead>
        </Table>

        <Modal size="md" isOpen={this.state.modal} className={this.props.className}>
          <ModalHeader>{this.state.item.name} {'      '} <small>${this.state.oldpriceitem}</small></ModalHeader>
          <ModalBody>
            {/* <Row>
                {this.props.favourListCart?this.props.favourListCart.map((item, index)=> (
                  <Col xs="12" sm="12"  className="favorItem" md="12" lg="12" key={item.id}>
                    <ListGroup>
                        <ListGroupItem className="list-item">{item.name} </ListGroupItem>
                        <Row className="model-row">
                        {item.list?item.list.map((list)=> (
                                <Col xs="12" sm="3" md="3" lg="3" className="margin020" key={list.id}>
                                  {item.type=="check"? 
                                            <FormGroup check>
                                                <Label check>
                                                    <Input type="checkbox" checked={list.checked}  onChange={e => this.handleChangeCart(e, list, this.state.datamodelcart, item.name, index, item)}/>{' '}
                                                    <p  className="pull-left">{list.name} &nbsp;&nbsp;&nbsp;&nbsp;<span className="pull-right">${list.price}</span></p>
                                                </Label>
                                            </FormGroup>
                                            :<FormGroup check>
                                            <Label check>
                                            <Input type="radio" checked={list.checked}  value={list.id} onChange={e => this.handleChangeCart(e, list, this.state.datamodelcart, item.name, index, item)} name={item.name} />{' '}
                                            {list.name} &nbsp;&nbsp;&nbsp;&nbsp;<span className="pull-right">${list.price}</span>
                                            </Label>
                                        </FormGroup>    
                                    }
                                </Col>
                            )):null}
                        </Row>
                    </ListGroup>
                    </Col>
                )):null}
            </Row> */}
            <Row>
            <Table>
                  <thead>
                  <tr>
                      <th>Name</th>
                      <th>Qty</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                    <tbody>
                    {this.state.item.favours?this.state.item.favours.map((favor, index)=> (
                        <tr key={favor.id}>
                            <td>{favor.name}</td>
                            <td>${favor.price}</td>
                            <td>
                            <Button onClick={() => that.deletefavor(favor, this.props.cartList)} className="float-left delete-btn boder-radius" color="danger"><i className="fa fa-close" /></Button>
                            </td>
                        </tr>
                      )):null}
                    </tbody>
                  <thead>
                  </thead>
              </Table>
            </Row>
          </ModalBody>
          <ModalFooter> 
              <span className="padding-right10">${this.state.itemtotalprice}</span>{'  '}
              <Button onClick={this.handleClickAdd.bind(this)} className="float-left btn-padding boder-radius no-margin" color="primary"><i className="fa fa-plus" /></Button>
              <Input className="float-left count-input boder-radius no-margin" value={this.state.item.qty} onChange={() => this.handlecount()} readOnly placeholder="0" />
              <Button onClick={this.handleClickRemove.bind(this)} className="boder-radius btn-padding no-margin" color="danger"><i className="fa fa-minus" /></Button>{' '}
              <Button color="primary" onClick={() => this.editcartdata(this.state.item)}>Update</Button>
              <Button color="danger" onClick={() => this.openModel(this.state.item)}>Cancel</Button>
          </ModalFooter>
          </Modal>
      
      </div>
    )
  }
}
const mapStateToProps = (state) => {
 	return {
    cartList: state.Cart.carts,
    totalprice: state.Cart.totalprice,
    favourListCart:state.Cart.favour,
	}
}
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		cartlist,edit_cart,delete_cart_one, delete_cart_all, orderdone, favor_update, getFavour
	},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Receipt);