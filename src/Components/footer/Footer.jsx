import React, { Component } from 'react'
import './Footer.css'

export default class Footer extends Component {
  render() {
    return (
        <div>
            <footer className="footer">
                  <p className="text-center no-margin">Copyright @2019 | Designed by Irshad</p>
            </footer>
        </div>
    )
  }
}
