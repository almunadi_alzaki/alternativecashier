import React from 'react';
import Navbar from './Components/header/Header';
import Footer from './Components/footer/Footer';
import Home from './Components/home/Home';
import AdminLogin from './Admin/Login/Login';
import AdminHome from './Admin/home/Home';
import Adminkitchener from './Admin/kitchener/kitchener';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
library.add(faStroopwafel);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    var routesdata;
      var data = window.location.pathname.split("/");
      console.log(data, data[data.length-1]);
      if(data[1]=="cashier"){
          routesdata = <Router>
                          <Route exact path="/cashier" component={AdminLogin} />
                          <Route exact path="/cashier/home" component={AdminHome} />
                          <Route exact path="/cashier/kitchener" component={Adminkitchener} />
                      </Router>
                   
      }else{
          routesdata =<Router>
          <Navbar />
             <Route exact path="/" component={Home} />
          <Footer />
        </Router>
      }
    return (
      <div>
        {routesdata}
      </div>
    );
  }
}

export default App;
