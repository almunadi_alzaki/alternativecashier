import * as types from '../constants/ActionTypes';
import axios from 'axios';
// const url = 'http://139.162.35.128:8881/'
const url = 'http://localhost:8881/'
var cartArray =[];

// var db = openDatabase('foodApp', '1.0', 'Test DB', 2*1024*1024);


// db.transaction(function (tx) { 
//     tx.executeSql('CREATE TABLE IF NOT EXISTS alldata (id unique, data)'); 
//     // tx.executeSql('INSERT INTO LOG (id, log) VALUES (3, "foobar")'); 
//     // tx.executeSql('INSERT INTO LOG (id, log) VALUES (5, "logmsg")'); 
// });
    
if(JSON.parse(localStorage.getItem("cartdata"))!=null){
    cartArray = JSON.parse(localStorage.getItem("cartdata"));
}
export function cartlist(){
    var total = 0;
    if(JSON.parse(localStorage.getItem("cartdata"))==null){
        return {
            type:types.CART_LIST,
            payload:[],
            totalprice:total
        }
    }
    for(var i=0; i<JSON.parse(localStorage.getItem("cartdata")).length;i++){
        total = Number(total)+Number(JSON.parse(localStorage.getItem("cartdata"))[i].totelprice);
         if(i==JSON.parse(localStorage.getItem("cartdata")).length-1){
            return {
                type:types.CART_LIST,
                payload:JSON.parse(localStorage.getItem("cartdata")),
                totalprice:total
            }
        }
    }
}
export function add_cart(data){
    // delete data.favour;
    cartArray.push(data);
    localStorage.setItem("cartdata", JSON.stringify(cartArray));
    return {
        type:types.ADD_CART,
        payload:JSON.parse(localStorage.getItem("cartdata")),
    }
}

export function edit_cart(edit_data, index){
    var total = 0;
    cartArray.splice(index,1, edit_data);
    for(var i=0; i<cartArray.length;i++){
        total = Number(total)+Number(JSON.parse(localStorage.getItem("cartdata"))[i].price);
        if(i==cartArray.length-1){
            return {
                type:types.EDIT_CART,
                payload:cartArray,
                totalprice:total
            }
        }
    }
}

export function delete_cart_one(del_data, index){
    cartArray.splice(index,1);
    var total= 0;
    console.log(cartArray, index);
    if(cartArray.length!=0){
        localStorage.setItem("cartdata", JSON.stringify(cartArray));
        for(var i=0; i<JSON.parse(localStorage.getItem("cartdata")).length;i++){
            total = Number(total)+Number(JSON.parse(localStorage.getItem("cartdata"))[i].totelprice);
             if(i==JSON.parse(localStorage.getItem("cartdata")).length-1){
                return {
                    type:types.CART_LIST,
                    payload:JSON.parse(localStorage.getItem("cartdata")),
                    totalprice:total
                }
            }
        }
    }else{
        localStorage.removeItem("cartdata");
        return {
            type:types.CART_LIST,
            payload:[],
            totalprice:0
        }
    }
   
    // return {
    //     type:types.DELETE_CART_ONE,
    //     payload:JSON.parse(localStorage.getItem("cartdata"))
    // }
}

export function favor_update(array){
    localStorage.setItem("cartdata", JSON.stringify(array));
    return {
        type:types.FAVOR_UPDATE,
        payload:JSON.parse(localStorage.getItem("cartdata"))
    }
}

export function delete_cart_all(){
    cartArray=  [];
    localStorage.setItem("cartdata", null);
    return {
        type:types.DELETE_CART_ALL,
        payload:[],
        totalprice:0
    }
}

function margeFavorItem(data, callback){
    var datamain = data.datamain;
    var favorItem = data.favorItem;
    for(var d=0;d<datamain.length;d++){
        for(var i=0; i<datamain[d].item.length; i++){
            for(var f=0; f<datamain[d].item[i].favour.length; f++){
                for(var fi=0; fi<favorItem.length; fi++){
                    if(favorItem[fi].favor_id==datamain[d].item[i].favour[f].id){
                        datamain[d].item[i].favour[f].list.push({
                            "id":favorItem[fi]._id,
                            "name":favorItem[fi].name,
                            "price":favorItem[fi].price,
                            "favor_id":favorItem[fi].favor_id,
                            "created_date":favorItem[fi].created_date,
                        })
                    }
                }   
            }
        }
        if(d==datamain.length-1){
            callback(datamain);
        }
    }
    
   
}
function margeFavors(data, callback){
    var favor = data.favors;
    var datamain = data.datamain;
    for(var d=0;d<datamain.length;d++){
        for(var i=0; i<datamain[d].item.length; i++){
           for(var f=0;f<favor.length;f++){
            if(datamain[d].id==favor[f].cat_id && datamain[d].item[i].id==favor[f].item_id){
                datamain[d].item[i].favour.push({
                    "id":favor[f]._id,
                    "cat_id":favor[f].cat_id,
                    "item_id":favor[f].item_id,
                    "check":favor[f].check,
                    "created_date":favor[f].created_date,
                    "list":[],
                })
            }
           }
        }
        if(d==datamain.length-1){
            margeFavorItem({datamain:datamain, favorItem:data.favorItem}, function(data) {
                callback(data);
            });
        }
    }
}

function margeItem(data, callback) {
    var item = data.item;
    var datamain = data.mainArray;
    for(var d=0;d<datamain.length;d++){
        for(var i=0; i<item.length; i++){
            if(datamain[d].id==item[i].cat_id){
                datamain[d].item.push({
                    "id":item[i]._id,
                    "cat_id":item[i].cat_id,
                    "name":item[i].name,
                    "price":item[i].price,
                    "dis":item[i].discription,
                    "image":item[i].image,
                    "prepared_time":item[i].prepared_time,
                    "created_date":item[i].created_date,
                    "favour":[],
                })
            }
        }
        if(d==datamain.length-1){
            margeFavors({datamain:datamain, favors:data.favors, favorItem:data.favorItem}, function(data) {
                callback(data);
            });
        }
    }
    // 
            
}

export function getcategory(){
    // return function(dispatch) {
       var mainArray = [];
       let maindata = axios.get(url+'category/aggregationdata').then(async function (response) {
            for(var c=0;c<response.data.category.length;c++){
                mainArray.push({
                    "id":response.data.category[c]._id,
                    "name":response.data.category[c].name,
                    "created_date":response.data.category[c].created_date,
                    "item":[]
                });
                if(c==response.data.category.length-1){
                    margeItem({mainArray:mainArray, item:response.data.item, favors:response.data.favors, favorItem:response.data.favorItem}, function(data) {
                         return data;
                    });
                }
            }
        }).catch(function (error) {
            return undefined;
        });
    // }
    return {
        type:types.GET_CATEGORY,
        payload:maindata
    }
}

export function getItemList(data){
    return {
        type:types.GET_ITEM,
        payload:data
    }
}
export function getFavour(data){
    if(data==undefined){
        data= [];
    }
    return {
        type:types.GET_FAVOUR,
        payload:data
    }
}

export function orderdone(orderdata, price, status){
    console.log("auto load");
    var data = {
        order_status: status,
        items:orderdata,
        total_price:price
    }
   
    if(localStorage.getItem("orderId")!=undefined || localStorage.getItem("orderId")!=null){
        data._id = localStorage.getItem("orderId");
        console.log(data);
        var headers = {
            'Content-Type': 'application/json',
          }
          let catdata =  axios.put(url+'orders/update/'+localStorage.getItem("orderId"), data, {headers: headers}).then(function (response) {
              return response.data.data.msg;
           }).catch(function (error) {
              return error;
           });
           localStorage.removeItem("orderId");
          return {
              type:types.ORDER_DONE,
              payload:catdata
          }
    }else{
        var headers = {
            'Content-Type': 'application/json',
          }
          let catdata =  axios.post(url+'orders', data, {headers: headers}).then(function (response) {
              return response.data.data.msg;
           }).catch(function (error) {
              return error;
           });
          return {
              type:types.ORDER_DONE,
              payload:catdata
          }
    }
}

export function orderlist(data){
    return function(dispatch) {
        var headers = {
        'Content-Type': 'application/json',
        }
        axios.post(url+'orders/confirmorder', data, {headers: headers}).then(function (response) {
            console.log(response);
            if(response.data==undefined || response.data==null){
                dispatch({
                    type: types.ORDER_LIST,
                    payload: []
                });
            }else{
                dispatch({
                    type: types.ORDER_LIST,
                    payload: response.data
                });
            }
        }).catch(function (error) {
            dispatch({
                type: types.ORDER_LIST,
                payload: []
            });
        });
    }
}


export function orderconfirmlist(data){
    return function(dispatch) {
        var headers = {
        'Content-Type': 'application/json',
        }
        axios.post(url+'orders/confirmorder', data, {headers: headers}).then(function (response) {
            console.log(response);
            if(response.data==undefined || response.data==null){
                dispatch({
                    type: types.ORDER_CONFIRM_LIST,
                    payload: []
                });
            }else{
                dispatch({
                    type: types.ORDER_CONFIRM_LIST,
                    payload: response.data
                });
            }
        }).catch(function (error) {
            dispatch({
                type: types.ORDER_CONFIRM_LIST,
                payload: []
            });
        });
    }
}


export function ordercartlist(data){
    console.log(data);
    cartArray = data.items;
    console.log(cartArray);
    localStorage.setItem("orderId", data._id);
    localStorage.setItem("cartdata", JSON.stringify(cartArray));
    var total = 0;
    for(var i=0; i<cartArray.length;i++){
        total = Number(total)+Number(cartArray[i].totelprice);
         if(i==cartArray.length-1){
            return {
                type:types.CART_LIST,
                payload:data.items,
                totalprice:total
            }
        }
    }
}

export function userLogin(email, password){

}

export function orderupate(order, orderArray){
   return function(dispatch) {
        var headers = {
            'Content-Type': 'application/json',
        }
        axios.put(url+'orders/update/'+order._id, order, {headers: headers}).then(function (response) {
            console.log(response);
            if(response.data==undefined || response.data==null){
                dispatch({
                    type:types.ORDER_LIST,
                    payload: []
                });
            }else{
                var index = orderArray.indexOf(order);
                orderArray.splice(index,1);
                console.log(orderArray);
                dispatch({
                    type: types.ORDER_LIST,
                    payload: orderArray
                });
            }
            
        }).catch(function (error) {
            dispatch({
                type: types.ORDER_LIST,
                payload: []
            });
        });
    }
}