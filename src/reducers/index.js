import {combineReducers} from 'redux';
import Cart from './cart_reducers'
const rootReducer = combineReducers({
    Cart
})

export default rootReducer;