

export default function (state = {}, action) {
    switch(action.type){
        case 'GET_CATEGORY':
           return { ...state, category:action.payload}
        case 'GET_ITEM':
           return { ...state, items:action.payload}
        case 'GET_FAVOUR':
           return { ...state, favour:action.payload}
        case 'ORDER_DONE':
           return { ...state, order:action.payload}
        case 'CART_LIST':
            return { ...state, carts:action.payload, totalprice:action.totalprice}
        case 'ADD_CART':
          return { ...state, carts:action.payload}
		case 'EDIT_CART':
          return  { ...state, carts:action.payload, totalprice:action.totalprice}
        case 'DELETE_CART_ALL':
            return { ...state, carts:action.payload, totalprice:action.totalprice}
        case 'DELETE_CART_ONE':
            return { ...state, carts:action.payload}
        case 'FAVOR_UPDATE':
            return { ...state, carts:action.payload}
        case 'ORDER_LIST':
            return { ...state, orders:action.payload}
        case 'USER_LOGIN':
            return { ...state, userdata:action.payload}
        case 'ORDER_CONFIRM_LIST':
        return { ...state, orderconfirm:action.payload}
            
		
        default :
        return state;
    }
}