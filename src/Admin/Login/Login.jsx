import React from 'react'
import { Button, Form, FormGroup, Input } from 'reactstrap';
import './Login.css'
import { connect } from "react-redux";
import {bindActionCreators} from 'redux';
import { userLogin } from '../../actions'
import axios from 'axios';
const url = 'http://localhost:8881/'
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            password:''
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.userLoginMethod = this.userLoginMethod.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    userLoginMethod = () =>{
        if(this.state.email==''){
            alert("required Email.");
        }else if(this.state.password==''){
            alert("required Password.");
        }else{
            const that = this;
            var headers = {
            'Content-Type': 'application/json',
            }
            axios.post(url+'user/login', {email: this.state.email, password: this.state.password}, {headers: headers}).then(function (response) {
                console.log(response);
                if(response.data==undefined || response.data==null){
                   alert("User not exist");
                }else{
                    if(response.data.isLogin){
                        if(that.state.email=="admin@gmail.com"){
                            window.location.href = "/cashier/home";
                        }else{
                            window.location.href = "/cashier/kitchener";
                        }
                    }else{
                        alert(response.data.msg);
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        //    var response =  this.props.userLogin(this.state.email, this.state.password);
        //    window.location.href = "/cashier/home";
        }
    }

    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    render() {
    return (
        <div className="wrapper main-div">
            <div id="formContent">
                <div className="first">
                 <h2>
                  Alternative Cashier
                </h2>
                </div>
                <div className="padding20">
                    <Form>
                        <FormGroup>
                        <Input type="email" name="email" value={this.state.email} placeholder="Email" onChange={this.handleChangeEmail} />
                        </FormGroup>
                        <FormGroup>
                        <Input type="password" name="password" value={this.state.password} placeholder="*************"  onChange={this.handleChangePassword} />
                        </FormGroup>
                        {/* <Link to="/cashier/home">  */}
                        
                        {/* </Link> */}
                    </Form>
                    <Button type="submit" value="submit" onClick={() => this.userLoginMethod()} className="btn-submit">Submit</Button>
                </div>
                {/* <div id="formFooter">
                <a class="underlineHover" href="#">Forgot Password?</a>
                </div> */}
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        UserData:state.Cart.user
    }
  }
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        userLogin
    },dispatch)
    
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(Login)
