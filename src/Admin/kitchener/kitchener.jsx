import React from 'react'
import { Card, CardImg, CardText, CardBody, CardLink,
    CardTitle, CardSubtitle, Collapse, ListGroup, ListGroupItem, Container, FormGroup, Label, Row, Col, Table, Button, Input, Modal, ModalHeader, ModalBody, ModalFooter, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import './kitchener.css'
import { Link } from 'react-router-dom';
import { orderlist, orderupate } from '../../actions';
import { connect } from "react-redux";
import {bindActionCreators}from 'redux';
import AdminFooter from '../footer/Footer';

class kitchener extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            itemvalue:[],
            collapse:false,
            total_price:0,
            maindata:{}
          };
    }

    componentDidMount () {
        this.props.orderlist({order_status: "confirm"});
    }
    
    showDetail = (value) =>{
      console.log(value);
      this.state.itemvalue = value.items;
        this.setState(prevState => ({
            modal: true,
            itemvalue: this.state.itemvalue,
            total_price:value.total_price,
            maindata:value
        }));
    }
    cancelModel = () =>{
        this.setState(prevState => ({
            modal: false,
            itemvalue:[],
            maindata:{}
        }));
    }

    Finish = () =>{
      this.state.maindata.order_status = "Finish";
      this.props.orderupate(this.state.maindata, this.props.orderList);
      this.setState(prevState => ({
          modal: false,
          itemvalue:[],
          maindata:{}
      }));
    }
    Deliver = () =>{
      this.state.maindata.order_status = "Deliver";
      this.props.orderupate(this.state.maindata, this.props.orderList);
      this.setState(prevState => ({
          modal: false,
          itemvalue:[],
          maindata:{}
      }));
    }
    
  render() {
    const that = this;
    return (
      <div>
          <Navbar color="faded" className="padding" light fixed='top' expand="md">
            <NavbarBrand><Link  to="/cashier/kitchener">Alternative kitchener</Link></NavbarBrand>
            </Navbar>
        <div className="mb-50">
        <Table className="margintop50">
        <thead>
          <tr  className="text-center">
            <th>#</th>
            <th>Order Id</th>
            <th>Total Price</th>
          </tr>
        </thead>
        <tbody>
            {this.props.orderList?this.props.orderList.map((item, index)=> (
                <tr key={item._id}  onClick={()=> this.showDetail(item)} className="text-center">
                    <th scope="row">{index+1}</th>
                    <td>#{item.order_number}</td>
                    <td>${item.total_price}</td>
                </tr>
            )):null}
        </tbody>
      </Table>
      <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
          <ModalBody className="no-padding">
          <Row className="orderItemHeader">
              <Col xs="3" sm="3" md="3" lg="3">
              #
              </Col>
              <Col xs="3" sm="3" md="3" lg="3">
                Name
              </Col>
              <Col xs="3" sm="3" md="3" lg="3">
                Qty
              </Col>
              <Col xs="3" sm="3" md="3" lg="3">
                Price
              </Col>
          </Row>
          {this.state.itemvalue?this.state.itemvalue.map((item, index)=> (
              <Row key={item._id} className="orderItem">
                <Col xs="3" sm="3" md="3" lg="3">
                  {index+1}
                </Col>
                <Col xs="3" sm="3" md="3" lg="3">
                {item.name}
                </Col>
                <Col xs="3" sm="3" md="3" lg="3">
                  {item.qty}
                </Col>
                <Col xs="3" sm="3" md="3" lg="3">
                ${item.price}
                </Col>
                {item.favours.length!=0?<span className="favortitle">Favour List</span>:''}
                {item.favours?item.favours.map((itemfavor, index)=> (
                  <Row key={itemfavor._id} className="orderItemHeaderfavor">
                      <Col xs="4" sm="4" md="4" lg="4">
                        {index+1}
                      </Col>
                      <Col xs="4" sm="4" md="4" lg="4">
                      {itemfavor.name}
                      </Col>
                      <Col xs="4" sm="4" md="4" lg="4">
                      ${itemfavor.price}
                      </Col>
                    </Row>
                )):null}
              </Row>
            )):null}
          
          </ModalBody>
          <ModalFooter>
            <span>Totoal Price : ${this.state.total_price}</span>
            <Button color="primary" onClick={() => this.Finish()}>Finish</Button>{' '}
            <Button color="primary" onClick={() =>this.Deliver()}>Deliver</Button>{' '}
            <Button color="danger" onClick={() => this.cancelModel()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
      <AdminFooter />
    </div>
      )
  }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        orderList:state.Cart.orders,
    }
  }
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        orderlist, orderupate
    },dispatch)
    
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(kitchener)
