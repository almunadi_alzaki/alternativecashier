import React, { Component } from 'react'
import { Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem, NavbarBrand } from 'reactstrap';
  import {connect} from 'react-redux';
import { getcategory, getItemList } from '../../actions';
import { Link } from 'react-router-dom';
import {bindActionCreators}from 'redux';
import './Header.css'
import axios from 'axios';
// const url = 'http://139.162.35.128:8881/'
const url = 'http://localhost:8881/'
var sample;
function margeFavorItem(data, callback){
  var datamain = data.datamain;
  var favorItem = data.favorItem;
  for(var d=0;d<datamain.length;d++){
      for(var i=0; i<datamain[d].item.length; i++){
          for(var f=0; f<datamain[d].item[i].favour.length; f++){
              for(var fi=0; fi<favorItem.length; fi++){
                  if(favorItem[fi].favor_id==datamain[d].item[i].favour[f].id){
                      datamain[d].item[i].favour[f].list.push({
                          "id":favorItem[fi]._id,
                          "name":favorItem[fi].name,
                          "price":favorItem[fi].price,
                          "favor_id":favorItem[fi].favor_id,
                          "created_date":favorItem[fi].created_date,
                      })
                  }
              }   
          }
      }
      if(d==datamain.length-1){
          callback(datamain);
      }
  }
  
 
}
function margeFavors(data, callback){
  var favor = data.favors;
  var datamain = data.datamain;
  for(var d=0;d<datamain.length;d++){
      for(var i=0; i<datamain[d].item.length; i++){
         for(var f=0;f<favor.length;f++){
          if(datamain[d].id==favor[f].cat_id && datamain[d].item[i].id==favor[f].item_id){
              datamain[d].item[i].favour.push({
                  "id":favor[f]._id,
                  "cat_id":favor[f].cat_id,
                  "item_id":favor[f].item_id,
                  "check":favor[f].check,
                  "name":favor[f].name,
                  "type":favor[f].check==true?"check":'radio',
                  "created_date":favor[f].created_date,
                  "list":[],
              })
          }
         }
      }
      if(d==datamain.length-1){
          margeFavorItem({datamain:datamain, favorItem:data.favorItem}, function(data) {
              callback(data);
          });
      }
  }
}

function margeItem(data, callback) {
  var item = data.item;
  var datamain = data.mainArray;
  for(var d=0;d<datamain.length;d++){
      for(var i=0; i<item.length; i++){
          if(datamain[d].id==item[i].cat_id){
              datamain[d].item.push({
                  "id":item[i]._id,
                  "cat_id":item[i].cat_id,
                  "name":item[i].name,
                  "price":item[i].price,
                  "dis":item[i].discription,
                  "image":item[i].image,
                  "prepared_time":item[i].prepared_time,
                  "created_date":item[i].created_date,
                  "favour":[],
              })
          }
      }
      if(d==datamain.length-1){
          margeFavors({datamain:datamain, favors:data.favors, favorItem:data.favorItem}, function(data) {
              callback(data);
          });
      }
  }  
}

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      active:1,
      datanav:[]
    };
    this.clicktoactive = this.clicktoactive.bind();
  }

  componentDidMount () {
    var mainArray = [];
    const that = this;
    axios.get(url+'category/aggregationdata').then(async function (response) {
        for(var c=0;c<response.data.category.length;c++){
            mainArray.push({
                "id":response.data.category[c]._id,
                "name":response.data.category[c].name,
                "created_date":response.data.category[c].created_date,
                "item":[]
            });
            if(c==response.data.category.length-1){
                margeItem({mainArray:mainArray, item:response.data.item, favors:response.data.favors, favorItem:response.data.favorItem}, function(data) {
                  that.props.getItemList(data[0].item);  
                  that.setState({
                        active: data[0].id,
                        datanav: data
                    });
                });
            }
        }
    }).catch(function (error) {
      console.log(error);
    });
  }
  clicktoactive= (value) =>{
    this.setState({
        active: value.id
    });
    this.props.getItemList(value.item);
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {
    return (
      <div className="mb-50">
            <Navbar color="faded" light fixed='top' expand="md">
            <NavbarBrand><Link  href="/" to="/">Alternative Cashier</Link></NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
               {this.state.datanav?this.state.datanav.map((item, index)=> (
                  <NavItem key={index} className={this.state.active===item.id ? 'activeli':''} onClick={() => this.clicktoactive(item)}>
                      <Link to="/">{item.name}</Link>
                  </NavItem>
                )):null}
              </Nav>
            </Collapse>
          </Navbar>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
	return {
    // Category: state.Cart.category
	}
}
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		getcategory, getItemList
	},dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);