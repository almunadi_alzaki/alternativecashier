import React from 'react'
import { Card, CardImg, CardText, CardBody, CardLink,
    CardTitle, CardSubtitle, Collapse, ListGroup, ListGroupItem, Container, FormGroup, Label, Row, Col, Table, Button, Input, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import './Home.css'
import { add_cart, cartlist, getFavour } from '../../actions';
import Receipt from '../Receipt/Receipt';
import { connect } from "react-redux";
import {bindActionCreators}from 'redux';
import AdminNavbar from '../header/Header';
import AdminFooter from '../footer/Footer';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 1,
            modal: false,
            addClass:false,
            collapse: 1,
            itemtotalprice:0,
            oldpriceitem:0,
            calculate:0,
            itemold:{},
            datamodel:{},
            cartList:[],
            favourprice : 0,
            oldFaverprice:0
        }
        this.handlecount = this.handlecount.bind(this);
        this.handleClickRemove = this.handleClickRemove.bind(this);
        this.handleClickAdd = this.handleClickAdd.bind(this);
        this.toggle = this.toggle.bind(this);
        this.toggleclass = this.toggleclass.bind(this);
        this.addcartdata = this.addcartdata.bind(this);
        this.openCollapse = this.openCollapse.bind(this);
    }

    componentDidMount () {
    }
  
    openCollapse(value) {
        this.setState(state => ({ collapse: value }));
    }
    
    addcartdata = (item, qty) => {
      item.qty = qty;
    //   console.log(this.state.itemtotalprice);
      item.price = this.state.oldpriceitem;
      item.totelprice = Number(Number(this.state.datamodel.price)*Number(this.state.count))+Number(Number(this.state.favourprice)*Number(this.state.count));
      this.state.cartList.push(item);
      this.setState(prevState => ({
        datamodel: {},
        modal: !prevState.modal,
        count: 1,
        cartList: this.state.cartList,
        calculate:this.state.itemtotalprice
      }));
      this.props.add_cart(this.state.datamodel);
      this.props.cartlist();
    }
    toggle = (item, qty) => {
        this.props.getFavour(item.favour);
        item.qty = qty;
        item.favours = [];
        this.setState(prevState => ({
          datamodel: item,
          itemtotalprice:Number(item.price),
          oldpriceitem:Number(item.price),
          modal: !prevState.modal,
          count: 1,
        }));
    }

    togglecancel = () => {
        this.setState(prevState => ({
          datamodel: {},
          modal: !prevState.modal,
          count: 0,
        }));
    }
    toggleclass() {
        this.setState({
            addClass: !this.state.addClass
        });
    }
    handlecount(e) {
      this.setState({count:e.target.value});
    }
    handleClickAdd() {
        this.state.itemtotalprice += Number(this.state.calculate);
        this.setState((prevState) => ({
            count: prevState.count + 1,
            itemtotalprice: this.state.itemtotalprice
        }));
    }
    handleClickRemove() {
        if(this.state.count!=1){
            console.log(this.state.calculate);
            this.state.itemtotalprice -= Number(this.state.calculate);
            console.log(this.state.itemtotalprice);
            this.setState((prevState) => ({
                count: prevState.count - 1,
                itemtotalprice: this.state.itemtotalprice
            }));
        }
    }
    handleChange(e, value, item, radioname, index, checktype) {
        if(checktype.type=="check"){
            if(e.target.checked==true){
                value.checked = e.target.checked;
                value.title= radioname;
                if(Number(value.price)!=0){
                    if(this.state.count!=0){
                       this.state.itemtotalprice =Number(Number(value.price)*Number(this.state.count))+Number(this.state.itemtotalprice);
                    }else{
                        this.state.itemtotalprice = Number(this.state.itemtotalprice)+Number(value.price);
                    }
                }
                item.favours.push(value);
                this.state.favourprice += Number(value.price);
                this.setState(prevState => ({
                    datamodel: item,
                    favourprice:this.state.favourprice,
                    itemtotalprice: this.state.itemtotalprice,
                    calculate:this.state.itemtotalprice,
                }));
            }else{
                value.checked = e.target.checked;
                var index = item.favours.indexOf(value);
                if(Number(value.price)!=0){
                    if(this.state.count!=0){
                        this.state.itemtotalprice = Number(this.state.itemtotalprice)-Number(Number(value.price)*Number(this.state.count));
                       }else{
                        this.state.itemtotalprice = Number(this.state.itemtotalprice)-Number(value.price);
                       }
                }
                item.favours.splice(index,1);
                this.state.favourprice -= Number(value.price)
                this.setState(prevState => ({
                    datamodel: item,
                    favourprice:this.state.favourprice,
                    itemtotalprice: this.state.itemtotalprice,
                    calculate:this.state.itemtotalprice,
                }));
            }
        }
        else{
            if(item.favour[index].name===radioname){
               var check = this.state.datamodel.favours.some(el => el.title === radioname);
               if(!check){
                   value.checked = true;
                   value.title= radioname;
                   if(Number(value.price)!=0){
                       if(this.state.count!=0){
                        this.state.itemtotalprice = Number(this.state.itemtotalprice)+Number(Number(value.price)*Number(this.state.count));
                       }else{
                        this.state.itemtotalprice = Number(this.state.itemtotalprice)+Number(value.price);
                       }
                   }
                   item.favours.push(value);
                   this.state.favourprice += Number(value.price)
                   this.setState(prevState => ({
                       datamodel: item,
                       oldFaverprice:value.price,
                       favourprice:this.state.favourprice,
                       itemtotalprice: this.state.itemtotalprice,
                       calculate:this.state.itemtotalprice,
                   }));
               }else{
                   console.log("remove");
                   value.checked = e.target.checked;
                   value.title= radioname;
                   var index = item.favours.indexOf(value);
                   if(this.state.count!=0){
                    this.state.itemtotalprice = Number(this.state.oldpriceitem)+Number(Number(value.price)*Number(this.state.count));
                   }else{
                    this.state.itemtotalprice = Number(this.state.oldpriceitem)+Number(value.price);
                   }
                   item.favours.splice(index,1, value);
                   console.log(Number(this.state.favourprice)-Number(value.price),  Number(value.price) ,Number(this.state.favourprice));
                   if(this.state.oldFaverprice!=0){
                    this.state.favourprice = Number(Number(this.state.favourprice)-Number(this.state.oldFaverprice))+Number(value.price);
                   }else{
                    this.state.favourprice = Number(this.state.favourprice)-Number(value.price);
                   }
                   this.setState(prevState => ({
                       datamodel: item,
                       oldFaverprice:value.price,
                       favourprice:this.state.favourprice,
                       itemtotalprice:this.state.itemtotalprice,
                       calculate:this.state.itemtotalprice,
                   }));
               }
           }
        }
    }
  render() {
    const that = this;
    return (
      <div className="mb-50 padding">
           <AdminNavbar />
            <Row>
               <Col xs="12" sm="12" md="9" lg="9" className="box-row no-padding">
                <Row className="padding-main-row">
                    {this.props.itemList?this.props.itemList.map((item)=> (
                        <Col xs="12" sm="6" md="6" lg="6" key={item.id}>
                            <Card>
                                <CardBody>
                                <CardImg src="../food.jpg" thumbnail="true" className="thumblie" alt="food image" />
                                <div className="thumblie-content">
                                    <CardTitle>{item.name} <CardSubtitle className="float-right margin-top0">${item.price} <span className="spanadd" onClick={() => that.toggle(item, that.state.count)}>Add <i className="fa fa-plus"></i></span></CardSubtitle></CardTitle>
                                    
                                    <CardText className="float-left">
                                    {item.dis} 
                                    </CardText>
                                </div>
                                </CardBody>
                            </Card>
                        </Col>    
                    )):null}
                </Row>
                <Modal size="lg" isOpen={this.state.modal} className={this.props.className}>
                <ModalHeader>{this.state.datamodel.name}</ModalHeader>
                <ModalBody>
                    <Row>
                       {this.props.favourList?this.props.favourList.map((item, index)=> (
                          <Col xs="12" sm="12" className="favorItem" md="12" lg="12" key={item.id}>
                            <ListGroup>
                                <ListGroupItem className="list-item">{item.name} </ListGroupItem>
                                <Row className="model-row">
                                {item.list?item.list.map((list)=> (
                                        <Col xs="3" sm="3" md="3" lg="3" className="margin020" key={list.id}>
                                         {item.type=="check"? 
                                                    <FormGroup check>
                                                        <Label check>
                                                            <Input type="checkbox" onChange={e => this.handleChange(e, list, this.state.datamodel, item.name, index, item)}/>{' '}
                                                            <p  className="pull-left">{list.name} &nbsp;&nbsp;&nbsp;&nbsp;<span className="pull-right">${list.price}</span></p>
                                                        </Label>
                                                    </FormGroup>
                                                    :<FormGroup check>
                                                    <Label check>
                                                    <Input type="radio" value={list.id} onChange={e => this.handleChange(e, list, this.state.datamodel, item.name, index, item)} name={item.name} />{' '}
                                                    {list.name} &nbsp;&nbsp;&nbsp;&nbsp;<span className="pull-right">${list.price}</span>
                                                    </Label>
                                                </FormGroup>    
                                            }
                                        </Col>
                                    )):null}
                                </Row>
                            </ListGroup>
                           </Col>
                        )):null}
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <span className="padding-right10">${Number(Number(this.state.datamodel.price)*Number(this.state.count))+Number(Number(this.state.favourprice)*Number(this.state.count))}</span>{'  '}
                    <Button onClick={this.handleClickAdd.bind(this)} className="float-left btn-padding boder-radius no-margin" color="primary"><i className="fa fa-plus" /></Button>
                    <Input className="float-left count-input boder-radius no-margin" value={this.state.count} onChange={this.handlecount} readOnly placeholder="0" />
                    <Button onClick={this.handleClickRemove.bind(this)} className="boder-radius btn-padding no-margin" color="danger"><i className="fa fa-minus" /></Button>{' '}
                    <Button color="primary" onClick={() => this.addcartdata(this.state.datamodel, that.state.count)}>Done</Button>
                    <Button color="danger" onClick={() => this.togglecancel()}>Cancel</Button>
                </ModalFooter>
                </Modal>
               </Col>
               <Col className="no-padding" xs="12" sm="12" md="3" lg="3">
                <Receipt />
               </Col>
            </Row>
            <AdminFooter />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        itemList:state.Cart.items,
        favourList:state.Cart.favour,
    }
  }
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
            add_cart, getFavour, cartlist
          },dispatch)
    
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(Home)
