import React from 'react'
import './Order.css'
import {connect} from 'react-redux';
import { ListGroup, ListGroupItem, Badge, FormGroup, Label, Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { orderlist, ordercartlist, orderupate, orderconfirmlist } from '../../actions';
import {bindActionCreators}from 'redux';

class Order extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ordernumber:Math.floor(Math.random()* (+1000 - +10)),
      item:{},
      itemindex:0,
      modal: false,
      itemtotalprice:0,
      oldpriceitem:0,
      datamodelcart:{}
    }
  };

  componentDidMount () {
    this.props.orderlist({order_status: "create"});
    this.props.orderconfirmlist({order_status: "confirm"});
  }

  editcartvalue = (data)=> {
    console.log(data);
    this.props.ordercartlist(data);
  }
 
  finishOrder = (item)  => {
    item.order_status = "confirm";
    this.props.orderupate(item, this.props.orderList);
    this.props.orderlist({order_status: "create"});
  }
  render(){
    const that = this;
    return (
      <div className="mb-50 padding-r">
        <Row>
          <Col  xs="12" sm="12" md="12" lg="12">
            <h3>Pending Order List </h3>
            <Table>
                <thead>
                <tr>
                    <th>Order No.</th>
                    <th>Total Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                  <tbody>
                    {this.props.orderList?this.props.orderList.map((item, index)=> (
                        <tr key={item._id}>
                            <td onClick={() => this.editcartvalue(item)}>
                                #{item.order_number}
                            </td>
                            <td onClick={() => this.editcartvalue(item)}>
                                ${item.total_price}
                            </td>
                            <td>
                              <Button onClick={() => that.finishOrder(item)} className="float-left delete-btn boder-radius" color="primary"><i className="fa fa-check" /></Button>
                            </td>
                        </tr>
                      )):null}
                  </tbody>
            </Table>
          </Col>
          <Col  xs="12" sm="12" md="12" lg="12">
            <h3>Confirm Order List </h3>
            <Table>
                <thead>
                <tr>
                    <th>Order No.</th>
                    <th>Total Price</th>
                </tr>
                </thead>
                  <tbody>
                    {this.props.orderconfirmList?this.props.orderconfirmList.map((item, index)=> (
                        <tr key={item._id}>
                            <td>
                                #{item.order_number}
                            </td>
                            <td>
                                ${item.total_price}
                            </td>
                        </tr>
                      )):null}
                  </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  console.log(state);
 	return {
    orderList: state.Cart.orders,
    orderconfirmList : state.Cart.orderconfirm
	}
}
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		orderlist, ordercartlist, orderupate, orderconfirmlist
	},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Order);