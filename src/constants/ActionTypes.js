export const CART_LIST = 'CART_LIST'
export const ADD_CART = "ADD_CART"
export const DELETE_CART_ALL = 'DELETE_CART_ALL'
export const DELETE_CART_ONE = 'DELETE_CART_ONE'
export const EDIT_CART = 'EDIT_CART'
export const GET_CATEGORY = 'GET_CATEGORY'
export const GET_ITEM = 'GET_ITEM'
export const GET_FAVOUR = 'GET_FAVOUR'
export const ORDER_DONE = 'ORDER_DONE'
export const FAVOR_UPDATE = 'FAVOR_UPDATE'
export const ORDER_LIST = 'ORDER_LIST'
export const USER_LOGIN = 'USER_LOGIN'
export const ORDER_CONFIRM_LIST = 'ORDER_CONFIRM_LIST'





